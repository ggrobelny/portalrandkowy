using Microsoft.EntityFrameworkCore;
using portalRandkowy.API.Models;

namespace portalRandkowy.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        public DbSet<Value> Values { get; set; }
    }
}